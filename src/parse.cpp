#include <stream9/json/parse.hpp>

#include <stream9/json/namespace.hpp>

#include <ostream>
#include <system_error>

#include <stream9/ranges.hpp>
#include <stream9/strings/stream.hpp>

namespace stream9::json {

using pos_t = stream9::safe_integer<ptrdiff_t, 0>;
using line_no_t = stream9::safe_integer<ptrdiff_t, 1>;
using column_t = stream9::safe_integer<ptrdiff_t, 0>;

static void
translate_error(std::system_error const& e, pos_t const pos)
{
    using namespace std::literals;

    auto const& ec = e.code();

    if (ec.category().name() == "boost.json"sv) {
        throw parse_error {
            static_cast<boost::json::error>(ec.value()),
            pos
        };
    }
}

json::value
parse(std::string_view s, json::storage_ptr sp/*= {}*/,
                          json::parse_options const& opt/*= {}*/)
{
    std::error_code ec;
    unsigned char temp[BOOST_JSON_STACK_BUFFER_SIZE];

    boost::json::parser p(storage_ptr(), opt, temp);
    p.reset(std::move(sp));

    pos_t pos = p.write(s, ec);
    if (ec) {
        throw parse_error {
            static_cast<boost::json::error>(ec.value()),
            pos
        };
    }

    try {
        return json::value { p.release() };
    }
    catch (std::system_error const& e) {
        translate_error(e, pos);

        throw;
    }
}

json::value
parse(std::istream& s, json::storage_ptr sp/*= {}*/,
                       json::parse_options const& opt/*= {}*/)
{
    if (!s.good()) return {};

    char buf[8192];
    std::error_code ec;
    unsigned char temp[BOOST_JSON_STACK_BUFFER_SIZE];

    boost::json::stream_parser p(storage_ptr(), opt, temp);
    p.reset(std::move(sp));

    pos_t pos = 0;

    while (s.good()) {
        s.read(buf, sizeof(buf));
        pos_t const n_read = s.gcount();

        pos += p.write(buf, n_read, ec);
        if (ec) {
            throw parse_error {
                static_cast<boost::json::error>(ec.value()),
                pos
            };
        }
    }

    try {
        return json::value { p.release() };
    }
    catch (std::system_error const& e) {
        translate_error(e, pos);

        throw;
    }
}


/*
 * class parse_error
 */
static bool
truncate_line(std::string& line, column_t& column, ptrdiff_t const length)
{
    if (rng::ssize(line) < length) return false;

    auto const bol = std::max<column_t>(column - 10, 0);
    auto const eol = std::max<column_t>(column + 30, rng::ssize(line));

    column = column == 0 ? column : static_cast<column_t>(10);

    line = line.substr(bol, eol - bol);

    return true;
}

static std::string
create_message(std::string& line, line_no_t line_no, column_t column)
{
    std::string result;
    str::ostream os { result };

    os << "line " << line_no << ", column " << column << "\n";

    auto const is_truncated = truncate_line(line, column, 40);
    auto n_space = column;

    if (is_truncated) {
        os << "..." << line << "...\n";
        n_space += 3;
    }
    else {
        os << line << "\n";
    }

    os << std::string(n_space, ' ') << "^----";


    return result;
}

static bool
is_stream_prematually_end(std::istream const& json, pos_t const pos, column_t eol)
{
    return json.eof() && eol <= pos;
}

parse_error::
parse_error(boost::json::error const& e, pos_t const pos)
    : std::runtime_error { std::error_code(e).message() }
    , m_errc { e }
    , m_pos { pos }
{}

std::string parse_error::
format_message(std::string_view json) const
{
    if (m_pos > json.size()) return {};

    line_no_t line_no = 1;
    pos_t bol = 0;

    for (pos_t i = 0; i < m_pos; ++i) {
        if (json[i] == '\n') {
            bol = i + 1;
            ++line_no;
        }
    }

    pos_t eol = rng::ssize(json);

    for (pos_t i = m_pos; i < rng::ssize(json); ++i) {
        if (json[i] == '\n') {
            eol = i;
            break;
        }
    }

    assert(bol <= m_pos);
    assert(bol <= eol);

    std::string line { json.substr(bol, eol - bol) };

    column_t column = m_pos - bol;

    return create_message(line, line_no, column);
}

std::string parse_error::
format_message(std::istream& json) const
{
    json.clear();
    json.seekg(0);

    line_no_t line_no = 1;
    pos_t bol = 0;

    std::string line;
    while (std::getline(json, line)) {
        if (json.eof()) break;
        if (json.tellg() >= m_pos) break;

        ++line_no;

        bol = static_cast<std::streamoff>(json.tellg());
    }

    auto const eol = bol + line.size();

    if (is_stream_prematually_end(json, m_pos, eol)) {
        return {};
    }

    column_t column = m_pos - bol;

    return create_message(line, line_no, column);
}

void
tag_invoke(json::value_from_tag, json::value& v, parse_error const& e)
{
    auto& obj = v.emplace_object();

    obj["type"] = "stream9::json::parse_error";
    obj["code"] = std::error_code(e.code()).message();
    obj["pos"] = e.pos().value();
}

} // namespace stream9::json
