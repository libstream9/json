#ifndef STREAM9_JSON_BOOST_JSON_HPP
#define STREAM9_JSON_BOOST_JSON_HPP

#define BOOST_JSON_STANDALONE
#include <boost/json.hpp>

namespace stream9::json {

using kind = boost::json::kind;
using storage_ptr = boost::json::storage_ptr;

} // namespace stream9::json

#endif // STREAM9_JSON_BOOST_JSON_HPP
