#ifndef STREAM9_JSON_VALUE_FROM_HPP
#define STREAM9_JSON_VALUE_FROM_HPP

#include <filesystem>

#include "boost_json.hpp"
#include "has_tag_invoke.hpp"
#include "value.hpp"

namespace stream9::json {

struct value_from_tag : boost::json::value_from_tag
{
    value_from_tag() = default;
    value_from_tag(boost::json::value_from_tag) {}
};

namespace value_from_ {

    struct api
    {
        template<typename T>
        json::value
        operator()(T&& t, storage_ptr sp = {}) const
        {
            if constexpr (has_tag_invoke<T>) {
                json::value result { std::move(sp) };

                tag_invoke(value_from_tag(), result, std::forward<T>(t));

                return result;
            }
            else {
                return json::value {
                    boost::json::value_from(std::forward<T>(t), std::move(sp))
                };
            }
        }
    };

} // namespace value_from_

inline constexpr value_from_::api value_from;

inline void
tag_invoke(value_from_tag, json::value& v,
           std::same_as<std::filesystem::path> auto const& p) // prevent implicit conversion
{
    v = p.c_str();
}

} // namespace stream9::json

namespace boost::json {

template<typename T>
void
tag_invoke(value_from_tag, std::same_as<boost::json::value> auto& jv, T&& v)
    requires stream9::json::has_tag_invoke<T&&>
{
    stream9::json::value sv;

    // this can be reinterpret_cast boost::json::value to stream9::json::value
    // but let's take a safer way.
    tag_invoke(stream9::json::value_from_tag(), sv, std::forward<T>(v) );

    jv = std::move(sv);
}

inline void
tag_invoke(value_from_tag, std::same_as<boost::json::value> auto& jv,
           stream9::json::object const& v)
{
    jv = v;
}

inline void
tag_invoke(value_from_tag, std::same_as<boost::json::value> auto& jv,
           stream9::json::array const& v)
{
    jv = v;
}

inline void
tag_invoke(value_from_tag, std::same_as<boost::json::value> auto& jv,
           stream9::json::string const& v)
{
    jv = v;
}

} // namespace boost::json

#endif // STREAM9_JSON_VALUE_FROM_HPP
