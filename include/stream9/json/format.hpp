#ifndef STREAM9_JSON_FORMAT_HPP
#define STREAM9_JSON_FORMAT_HPP

#include "boost_json.hpp"

#include <initializer_list>
#include <string>
#include <string_view>

namespace stream9::json {

std::string
format(std::initializer_list<boost::json::value_ref>,
       std::string_view space = "");

} // namespace stream9::json

#endif // STREAM9_JSON_FORMAT_HPP
