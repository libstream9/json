#ifndef STREAM9_JSON_PARSE_HPP
#define STREAM9_JSON_PARSE_HPP

#include "boost_json.hpp"
#include "value_from.hpp"

#include <iosfwd>
#include <stdexcept>
#include <string>
#include <string_view>

#include <stream9/safe_integer.hpp>

namespace stream9::json {

class value;
using boost::json::parse_options;

json::value
    parse(std::string_view json,
          json::storage_ptr = {},
          json::parse_options const& = {});

json::value
    parse(std::istream& json,
          json::storage_ptr = {},
          json::parse_options const& = {});

class parse_error : public std::runtime_error
{
public:
    using pos_t = stream9::safe_integer<ptrdiff_t, 0>;

public:
    parse_error(boost::json::error const&, pos_t pos);

    // accessor
    auto code() const { return m_errc; }
    pos_t pos() const { return m_pos; }

    // query
    std::string format_message(std::string_view json) const;
    std::string format_message(std::istream& json) const;

private:
    boost::json::error m_errc;
    pos_t m_pos;
};

void tag_invoke(json::value_from_tag, json::value&, parse_error const&);

} // namespace stream9::json

#include "value.hpp"

#endif // STREAM9_JSON_PARSE_HPP
