#ifndef STREAM9_JSON_ARRAY_ITERATOR_HPP
#define STREAM9_JSON_ARRAY_ITERATOR_HPP

#include "namespace.hpp"

#include <compare>
#include <iterator>

#include <stream9/iterators.hpp>

namespace stream9::json {

template<typename It, typename Ref>
class array_iterator : public iter::iterator_facade<
                                        array_iterator<It, Ref>,
                                        std::random_access_iterator_tag,
                                        Ref,
                                        std::remove_reference_t<Ref>,
                                        std::iter_difference_t<It> >
{
public:
    array_iterator() = default;

    array_iterator(It const it)
        : m_it { it }
    {}

    template<typename It2, typename Ref2> // for conversion to const_iterator
    array_iterator(array_iterator<It2, Ref2> const it)
        : m_it { it.base() }
    {}

    It base() const { return m_it; }

private:
    friend class iter::iterator_core_access;

    Ref dereference() const
    {
        return reinterpret_cast<Ref>(*m_it);
    }

    void increment() { ++m_it; }
    void decrement() { --m_it; }

    void advance(std::iter_difference_t<array_iterator> const n)
    {
        m_it += n;
    }

    std::iter_difference_t<array_iterator>
        distance_to(array_iterator const& other) const
    {
        return other.m_it - m_it;
    }

    bool equal(array_iterator const& other) const
    {
        return other.m_it == m_it;
    }

    std::strong_ordering
        compare(array_iterator const& other) const
    {
        return other.m_it <=> m_it;
    }

private:
    It m_it;
};

} // namespace stream9::json

#endif // STREAM9_JSON_ARRAY_ITERATOR_HPP
