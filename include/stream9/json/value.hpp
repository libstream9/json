#ifndef STREAM9_JSON_VALUE_HPP
#define STREAM9_JSON_VALUE_HPP

#include "boost_json.hpp"
#include "has_tag_invoke.hpp"

#include <cstdint>
#include <string_view>
#include <system_error>

namespace stream9::json {

class string;
class array;
class object;

class value
{
public:
    using allocator_type = boost::json::value::allocator_type;

public:
    value() = default;
    explicit value(storage_ptr) noexcept;

    value(value const&) = default;
    value(value const&, storage_ptr);

    value(value&&) = default;
    value(value&&, storage_ptr);

    value(std::nullptr_t, storage_ptr = {}) noexcept;

    value(std::integral auto, storage_ptr = {}) noexcept;

    value(double, storage_ptr = {}) noexcept;

    template<std::convertible_to<std::string_view> T>
        requires (!has_tag_invoke<T>)
    value(T const&, storage_ptr = {});

    value(string const&, storage_ptr = {});
    value(string&&, storage_ptr = {});

    value(array const&, storage_ptr = {});
    value(array&&, storage_ptr = {});

    value(object const&, storage_ptr = {});
    value(object&&, storage_ptr = {});

    template<typename T>
        requires has_tag_invoke<T>
    value(T const&, storage_ptr = {});

    explicit value(boost::json::value const&, storage_ptr = {});
    explicit value(boost::json::value&&, storage_ptr = {});

    explicit value(boost::json::string const&, storage_ptr = {});
    explicit value(boost::json::string&&, storage_ptr = {});

    explicit value(boost::json::array const&, storage_ptr = {});
    explicit value(boost::json::array&&, storage_ptr = {});

    explicit value(boost::json::object const&, storage_ptr = {});
    explicit value(boost::json::object&&, storage_ptr = {});

    // assignment
    value& operator=(value const&) = default;
    value& operator=(value&&) = default;

    value& operator=(std::nullptr_t) noexcept;

    value& operator=(std::integral auto) noexcept;

    value& operator=(double) noexcept;

    value& operator=(std::convertible_to<std::string_view> auto const&);

    value& operator=(string const&);
    value& operator=(string&&);

    value& operator=(array const&);
    value& operator=(array&&);

    value& operator=(object const&);
    value& operator=(object&&);

    value& operator=(boost::json::value const&);
    value& operator=(boost::json::value&&);

    value& operator=(boost::json::string const&);
    value& operator=(boost::json::string&&);

    value& operator=(boost::json::array const&);
    value& operator=(boost::json::array&&);

    value& operator=(boost::json::object const&);
    value& operator=(boost::json::object&&);

    // accessor
    json::kind kind() const noexcept;

    bool get_bool() const noexcept;
    bool& get_bool() noexcept;

    int64_t get_int64() const noexcept;
    int64_t& get_int64() noexcept;

    uint64_t get_uint64() const noexcept;
    uint64_t& get_uint64() noexcept;

    double get_double() const noexcept;
    double& get_double() noexcept;

    string const& get_string() const noexcept;
    string&       get_string() noexcept;

    array const& get_array() const noexcept;
    array&       get_array() noexcept;

    object const& get_object() const noexcept;
    object&       get_object() noexcept;

    allocator_type get_allocator() const noexcept;
    storage_ptr const& storage() const noexcept;

    // query
    bool is_null() const noexcept;
    bool is_bool() const noexcept;
    bool is_int64() const noexcept;
    bool is_uint64() const noexcept;
    bool is_double() const noexcept;
    bool is_string() const noexcept;
    bool is_array() const noexcept;
    bool is_object() const noexcept;

    bool is_primitive() const noexcept;
    bool is_structured() const noexcept;

    // modifier
    void emplace_null();
    bool& emplace_bool();
    int64_t& emplace_int64();
    uint64_t& emplace_uint64();
    double& emplace_double();
    string& emplace_string();
    array& emplace_array();
    object& emplace_object();

    void swap(value& other);
    friend void swap(value& lhs, value& rhs);

    // conversion
    boost::json::value const& base() const;
    boost::json::value& base();

    operator boost::json::value const& () const;
    operator boost::json::value& ();

    template<typename T>
    T to_number(std::error_code& ec) const;

    template<typename T>
    T to_number() const;

    std::string to_string(std::string_view indent = "") const;

    // comparison
    bool operator==(value const&) const = default;

    bool operator==(std::nullptr_t) const noexcept;

    bool operator==(std::integral auto) const noexcept;

    bool operator==(double) const noexcept;

    bool operator==(std::convertible_to<std::string_view> auto const&) const noexcept;

    bool operator==(boost::json::value const&) const noexcept;
    bool operator==(boost::json::string const&) const noexcept;
    bool operator==(boost::json::array const&) const noexcept;
    bool operator==(boost::json::object const&) const noexcept;

    // serialization
    void serialize(std::ostream&, std::string_view indent = "") const;

    friend std::ostream& operator<<(std::ostream&, value const&);

protected:
    value(boost::json::string_kind_t, storage_ptr sp = {});
    value(boost::json::array_kind_t, storage_ptr sp = {});
    value(boost::json::object_kind_t, storage_ptr sp = {});

private:
    boost::json::value m_base;
};

namespace literals {

    json::value operator ""_js (char const* const ptr, size_t const len);

} // namespace literals

} // namespace stream9::json

#include "value.ipp"

namespace stream9::literals {

using namespace stream9::json::literals;

} // namespace stream9::literals

#include "array.hpp"
#include "object.hpp"
#include "string.hpp"

#endif // STREAM9_JSON_VALUE_HPP
