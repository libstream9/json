#ifndef STREAM9_JSON_ARRAY_HPP
#define STREAM9_JSON_ARRAY_HPP

#include "value.hpp"

#include "array_iterator.hpp"

#include <compare>
#include <concepts>
#include <initializer_list>
#include <iterator>

namespace stream9::json {

class array : public value
{
public:
    using allocator_type = value::allocator_type;
    using const_iterator = array_iterator<boost::json::array::const_iterator, value const&>;
    using const_pointer = const_iterator::pointer;
    using const_reference = std::iter_reference_t<const_iterator>;
    using const_reverse_iterator = array_iterator<boost::json::array::const_reverse_iterator, value const&>;
    using difference_type = std::iter_difference_t<const_iterator>;
    using iterator = array_iterator<boost::json::array::iterator, value&>;
    using pointer = iterator::pointer;
    using reference = std::iter_reference_t<iterator>;
    using reverse_iterator = array_iterator<boost::json::array::reverse_iterator, value&>;
    using size_type = boost::json::array::size_type;
    using value_type = value;

public:
    array();
    explicit array(storage_ptr);

    array(array const&) = default;
    array(array const&, storage_ptr);

    array(array&&) = default;
    array(array&&, storage_ptr);

    array(size_type count, storage_ptr = {});
    array(size_type count, value const&, storage_ptr = {});

    template<std::input_iterator It>
        requires std::constructible_from<value_type, std::iter_value_t<It>>
    array(It first, It last, storage_ptr = {});

    array(std::initializer_list<boost::json::value_ref>, storage_ptr = {});

    array& operator=(array const&) = default;
    array& operator=(array&&) = default;

    // accessor
    value const& at(size_type pos) const;
    value&       at(size_type pos);

    value const& operator[](size_type pos) const;
    value&       operator[](size_type pos);

    value const& front() const;
    value&       front();

    value const& back() const;
    value&       back();

    value const* data() const;
    value*       data();

    const_iterator begin() const;
    iterator       begin();

    const_iterator end() const;
    iterator       end();

    const_iterator cbegin() const;
    const_iterator cend() const;

    const_reverse_iterator rbegin() const;
    reverse_iterator       rbegin();

    const_reverse_iterator rend() const;
    reverse_iterator       rend();

    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    boost::json::array const& base() const;
    boost::json::array&       base();

    // query
    bool empty() const;

    size_type size() const;
    difference_type ssize() const;

    size_type max_size() const;

    // modifier
    void push_back(value const&);
    void push_back(value&&);

    template<typename Arg>
    value& emplace_back(Arg&&);

    iterator insert(const_iterator pos, value const&);
    iterator insert(const_iterator pos, value&&);
    iterator insert(const_iterator pos, size_type count, value const&);
    iterator insert(const_iterator pos, size_type count, value&&);

    template<std::input_iterator It>
        requires std::constructible_from<value_type, std::iter_value_t<It>>
    iterator insert(const_iterator pos, It first, It last);

    iterator insert(const_iterator pos,
                    std::initializer_list<boost::json::value_ref>);

    template<typename Arg>
    iterator emplace(const_iterator pos, Arg&&);

    void pop_back();

    iterator erase(const_iterator pos);
    iterator erase(const_iterator first, const_iterator last);

    void clear();

    void resize(size_type);
    void resize(size_type, value const&);

    void reserve(size_type);
    void shrink_to_fit();
};

} // namespace stream9::json

#include "array.ipp"

#endif // STREAM9_JSON_ARRAY_HPP
